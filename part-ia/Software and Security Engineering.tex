\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}

\title{Software and Security Engineering}
\author{Henry Caushi}
\date{July 2020}

\begin{document}

\maketitle

\section{Introduction}

Software engineering is not limited to the code and the ideas behind the software. Software developers have to produce software which is easy to understand, works well and is bug-free. Overly ambitious project requirements or a lack of developer competence can lead to massive financial loss or even loss of life. Numerous projects and their failures can teach us lessons about what it takes to develop good software.

Security engineering is a specific aspect of software engineering which focuses on how to keep software systems safe from threats. Such threats can be natural or malicious, from cybercriminals to hostile state actors. In order to counter them, security engineers must consider who might wish to do them harm, how they may do so, and what their capabilities are.

Software and Security Engineering combines aspects of both of these courses, illustrating what techniques are used by software and security engineers to produce software which works efficiently and reliably; to evaluate how people might try to exploit the software; and to deliver this software on time and within budget.

\section{Recommended Reading}

\begin{itemize}
    \item \emph{Security Engineering}, 3rd edition. 2020. Ross Anderson.
    \item More stuff coming soon!
\end{itemize}

\section{Security Engineering}

\subsection{Introduction}
Various things can go wrong with software. Broadly, these can be categorised into two major types: firstly, there are software errors. An internal state may occur which software developers never anticipated, causing a failure of the system. We'll go into these failures in Software Engineering. Secondly, there are threats to the software. These occur when an external force forces the software to go badly wrong in a way that is clearly not reasonable use of the software. For example, the following scenarios would come under security engineering: \begin{itemize}
    \item A flood destroys a data server, causing data to be lost
    \item A hacker abuses bugs in the code for malicious gain
    \item A terrorist attack causes significant damage to a server
    \item An electronic warfare aircraft jams a radar system, causing it to be unable to communicate with another
\end{itemize} Making software dependable involves considering the threats that it could face during its operation, and performing cost-benefit analysis of implementing the measures necessary to mitigate these threats.

\subsection{Vocabulary}

To begin, we must begin with some technical vocabulary. Many of the terms used are straightforward, but some are misleading or controversial.
\begin{itemize}
    \item A \emph{system} is anything which works as ``one''. It can be be a computer, a software product, the command and control infrastructure of a military, or a business and all its staff and customers, to name a few. Many failures stem from disagreement as to the definition of what the ``system'' is.
    \item An \emph{error} occurs when a system performs unintended behaviour due to a design flaw. A \emph{failure} occurs when the system unacceptably performs outside of its specifications.
    \item \emph{Reliability} is the ability of the system to keep operating in hostile conditions without failing to provide its expected service. For example, Bob should be able to access his files on a cloud storage website without it going down. It is often described in terms of mean time to failure (MTTF) or mean time before failure (MTBF).
    \item \emph{Security} describes how well the system stands up to threat actors who might want to use the software for malicious gain. The Chinese government should not be able to read or delete everyone's files, for example.
    \item \emph{Dependability} is a combination of reliability and security. It is one of the ultimate goals of a system.
    \item A \emph{subject} is a physical person in any role that is relevant to a system, including as an operator, a user, a threat actor, or a victim.
    \item A \emph{person} can be either a human or a legal entity, such as a company. This is a legal definition, so sadly it's the one we have to use.
    \item A \emph{principal} is any entity which participates in a security system. This could be a person or a piece of equipment. It could also be a port number, a cryptographic key, or a group of principals.
    \item A \emph{role} is a set of functions which (ordinarily) one person has at once, but which multiple people may have over time. For example, the ``Admiral of the USS Nimitz'' is a role currently performed by Captain Maximilian Clark.
    \item \emph{Identity} is the correlation of knowledge or context which makes it possible to recognise that two principals are the same. \emph{Anonymity} is when it's impossible to discover your identity.
    \item \emph{Trust} is when a subject is given escalation of privilege. It is different from \emph{trustworthiness}, which refers to a subject which won't cause a security failure. For example, an NSA employee who sells classified intelligence to a Chinese diplomat is trusted (because they had the information) but not trustworthy (because they gave it away).
    \item \emph{Secrecy} is when a system does not give principals any more information than is absolutely necessary to perform their necessary function.
    \item When used in the context of policy, \emph{confidentiality} is the legal or contractual obligation of a person to protect another person's secrets. When used in the context of secure communications, it refers to the quality that those communications cannot be read and understood by a bad actor.
    \item \emph{Privacy} is the ability and/or right to protect your own information and how it's used.
    \item \emph{Integrity} is the assurance that the data that has been written is actually written by the principal that says that they've written it. \emph{Authenticity} is the assurance of not only integrity, but also that the data is not a replay of previous communications.
    \item In the context of communications, \emph{availability} is the assurance that any communications sent will be received by the intended party.
    \item A \emph{vulnerability} is a feature of a system which can potentially lead to a failure. A \emph{threat} is a subject which has an interest in exploiting a vulnerability.
    \item A \emph{hazard} is an event or set of circumstances which can potentially cause a failure. An \emph{accident} is a realisation of such a hazard which does not involve a threat.
    \item In safety terms, a \emph{critical system} is one whose failure can lead to massive damage or loss of life.
    \item A \emph{safety case} is a statement of assurance that the system will not fail within its intended use environment.
    \item A \emph{security policy} is a strategy which describes how a person will protect their data, while a \emph{safety policy} is a strategy of how the system will be kept acceptably safe.
\end{itemize}

\subsection{Policy and Security}

\subsection{Safety and Psychology}

\subsection{Security Protocols}

Security protocols determine how information should be exchanged in such a way that systems remain secure. They usually involve some combination of access control, cryptography, and identification, but other principals can easily find their place in a security protocol. By using them, we can abstract how principals communicate and try to work any weaknesses in the protocol (or prove it to be secure).

As an extremely basic example, here is a protocol where Alice (\(A\)) and Bob (\(B\)) say hello to each other:
\begin{equation*}
\begin{split}
    A \longrightarrow B &: \mathrm{``Hello"} \\ 
    B \longrightarrow A &: \mathrm{``Hey"}
\end{split}
\end{equation*}

The first line that Alice sends the message ``Hello'' to Bob, who then responds with the message ``Hey''. This works perfectly well for getting the message across, but what if they wanted to send a secret message to each other. What if they were halfway across the globe, with Eve (\(E\)) and Mallory (\(M\)) who would stop at nothing to prevent their messages from safely reaching each other? In that case, they'll need to secure the message.

If they have a symmetric encryption key, they can send each other messages encrypted under that key. \begin{equation*}
\begin{split}
    A \longrightarrow B &: \{ \mathrm{``Hello"} \}_K \\ 
    B \longrightarrow A &: \{ \mathrm{``Hey"} \}_K
\end{split}
\end{equation*} If Eve were to intercept these messages, she would not be able to read them without the key, while if Mallory were to replace the ciphertext with a new message, Alice and Bob would easily be able to tell that the message has been tampered with (for abstraction, we assume that all algorithms are \emph{ideal}).

\end{document}
