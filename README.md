# Cambridge Computer Science notes

These are unofficial notes for the [Computer Science Tripos](https://www.cst.cam.ac.uk/teaching) at the University of Cambridge. They are currently provided by Jamie Cao and Henry Caushi.

While these notes are based on the courses, do note that they may also contain additional content which helps improve the context or understanding of the standard course material. They are correct at the time of writing, though do note that courses change and these notes may become badly outdated over time.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
